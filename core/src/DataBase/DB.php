<?php

namespace core\src\DataBase;

use PDO;


/**
 * BD is class abstraction for PDO
 *
 */
class DB
{

    /**
     * instance of PDO object
     *
     * @var PDO
     */
    public $instance = null;

    /**
     * Create a new BD instance.
     *
     * @return PDO
     */
    public function __construct()
    {
        return $this->getInstance();
    }

    /**
     * Return BD instance.
     *
     * @return PDO
     */
    public function getInstance(): PDO
    {

        if (empty($this->instance)) {
            return $this->createInstance();
        }

        return $this->instance;
    }

    /**
     * Get or create Instance
     *
     * @return PDO
     */
    protected function createInstance(): PDO
    {

        $dsn = "mysql:host=" . DBConfig::$host . ";dbname=" . DBConfig::$db . ";charset=" . DBConfig::$charset;
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        return new PDO($dsn, DBConfig::$user, DBConfig::$pass, $opt);
    }

}