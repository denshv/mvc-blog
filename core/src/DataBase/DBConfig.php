<?php

namespace core\src\DataBase;


/**
 * BDConfig init DBConfig
 *
 */

class DBConfig
{

    public static $user = null;

    public static $pass = null;

    public static $host = null;

    public static $db = null;

    public static $charset = 'utf8';

    private function __construct()
    {

    }

    /**
     * Static function set config params.
     *
     * @param string $user
     * @param string $pass
     * @param string $host
     * @param string $db
     *
     * @return void
     *
     */
    public static function setConfig($user, $pass, $host, $db)
    {
        self::$user = $user;
        self::$pass = $pass;
        self::$host = $host;
        self::$db = $db;
    }

}