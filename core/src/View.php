<?php

namespace core\src;


/**
 * Validator is a class that load templates and render content
 *
 */

class View
{
    /**
     * File type of template
     *
     * @var string
     */
    const FILE_TYPE = '.phtml';

    /**
     * Path to the template
     *
     * @var string
     */
    protected $path = null;

    /**
     * Name of view - template
     *
     * @var string
     */
    protected $viewName = null;

    /**
     * Array of params that will render in template
     *
     * @var array
     */
    protected $params = [];

    /**
     * Create a new View instance.
     *
     * @param string $viewName
     * @param array $params
     */
    public function __construct($viewName, $params = null)
    {
        $this->viewName = $viewName;
        $this->params = $params;
    }

    /**
     * Load template with rendered content
     *
     * @return void
     */
    public function display(): void
    {
        try {
            $this->load();

        } catch (\Exception $e) {

            echo $e->getMessage();
        }
    }

    /**
     * Load template with rendered content
     *
     * @throws \Exception if view template doesn't exist
     * @return void
     */
    public function load(): void
    {
        $path = VIEW_PATH . $this->viewName . self::FILE_TYPE;

        if (file_exists($path)) {

            if (!empty($this->params))
                extract($this->params, EXTR_PREFIX_SAME, "wddx");

            require_once $path;

        } else {
            throw new \Exception("View does not exist: " . $path);
        }

    }
}