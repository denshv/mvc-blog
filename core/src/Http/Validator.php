<?php

namespace core\src\Http;

/**
 * Validator is a class that check request on correct data
 *
 */
class Validator
{

    /**
     * Http Request
     *
     * @var array
     */
    protected $request = null;


    /**
     * Create a new Validator instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * Check list of params on empty
     *
     * @param array $requiredList
     *
     * @return boolean
     */
    public function check($requiredList): bool
    {

        foreach ($requiredList as $required) {

            if (empty($this->request->params->$required))
                return false;
        }

        return true;
    }

}