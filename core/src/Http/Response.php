<?php

namespace core\src\Http;

/**
 * Response is a class that simple implements HTTP Response
 *
 */

class Response
{
    protected function __construct()
    {

    }

    /**
     * Create message Object for responce
     *
     * @param string $message
     * @param boolean $status
     * @param mixed $content
     *
     * @return Object
     */
    public static function message($message, $status, $content = null): object
    {

        return (object)['status' => $status, 'msg' => $message, 'content' => array($content)];
    }
}