<?php

namespace core\src\Http;


/**
 * Router is a class that simple implements Routing
 *
 */

class Router
{

    /**
     * Params of Http Request
     *
     * @var array
     */
    protected $request = null;

    /**
     * Placeholder for сreating more readable and universal pattern
     *
     * @var array
     */
    protected $placeholders = [
        ':seg' => '([^\/]+)',
        ':num' => '([0-9]+)',
        ':any' => '(.+)'
    ];

    /**
     * Create a new Route instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Process route pattern with URI. Exec Closure if route-path-pattern matched with URI
     *
     * @param string $route
     * @param \Closure
     *
     * @return void
     */
    public function route($route, \Closure $func): void
    {
        $find = array_keys($this->placeholders);
        $replace = array_values($this->placeholders);

        if (strpos($route, ':') !== false) {
            $route = str_replace($find, $replace, $route);
        }

        $route .= '/?';

        $route = str_replace('/', '\/', $route);
        $is_match = preg_match('/^' . ($route) . '$/', $this->request->uri, $matches, PREG_OFFSET_CAPTURE);
        if ($is_match) {
            $func($matches);
        }
    }

    /**
     * Check is current route belongs to admin panel.
     *
     * @return boolean
     */
    public function isAdmin(): bool
    {
        if (strpos($this->request->uri, '/admin/') !== false)
            return true;

        return false;
    }

    /**
     * Include route map from path, run included anononimus function
     *
     * @param string
     * @return void
     */
    public function processMap($path): void
    {
        $func = require_once $path;

        $func($this, $this->request);
    }
}
