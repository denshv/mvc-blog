<?php

namespace core\src\Http;


/**
 * Request is a class that simple implements HTTP Request
 *
 */

class Request
{

    /**
     * Params of Http Request
     *
     * @var object
     */
    public $params = null;

    /**
     * Request URI
     *
     * @var string
     */
    public $uri = null;

    /**
     * Create a new Request instance.
     *
     */
    public function __construct()
    {
        $this->params = $this->getParams();

        $this->uri = $this->getURI();
    }

    /**
     * Return Request params as array
     *
     * @return object
     */

    protected function getParams():object
    {
        return (object)$_REQUEST;
    }

    /**
     * Return Request params as array
     *
     * @return string
     */
    protected function getURI(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Return Request params
     *
     * @return object
     */
    public function toObject(): object
    {
        return $this->params;
    }

}