<?php

namespace core\Helpers;

use core\src\DataBase\DB;
use core\src\DataBase\DBConfig;
use core\src\View;
use PDO;

/**
 * Helper function for view processing
 *
 * @param string
 * @param array
 *
 * @return void
 */
function view(string $viewName, array $params = []): void
{

    $view = new View($viewName, $params);
    $view->display();
}

/**
 * Helper function init DB, return DB instance
 *
 * @param string
 * @param string
 * @param string
 * @param string
 *
 * @return PDO
 */
function initDB(string $user, string $pass, string $hast, string $name): PDO
{

    DBConfig::setConfig($user, $pass, $hast, $name);

    $db = new DB();

    return $db->getInstance();
}

/**
 * Helper function return DB instance
 **
 * @return PDO
 */
function getDB(): PDO
{

    $db = new DB();

    return $db->getInstance();
}

/**
 * Helper function convert  array to object
 **
 * @return PDO
 */
function toObject($array): object
{

    $obj = array_map(function ($array) {
        return (object)$array;
    }, $array);

    return $obj;
}