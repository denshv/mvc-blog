<?php

namespace App\Controllers;

use App\Models\PostService;
use core\Helpers;

/**
 * Controller manage post actions from web routes
 */
class PostController
{

    /**
     * Get posts from model -> process result to view
     *
     * @return void
     */
    public function index(): void
    {
        $service = new PostService();

        $posts = $service->getAllPosts();

        Helpers\view('index', compact('posts'));
    }

    /**
     * Get post from model by id -> process result to view
     *
     * @param integer
     *
     * @return void
     */
    public function show($id): void
    {
        $id = (int)$id;
        $service = new PostService();
        $post = $service->getPostById($id);

        Helpers\view('details', compact('post'));

    }
}