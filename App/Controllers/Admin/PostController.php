<?php

namespace App\Controllers\Admin;

use App\Models\PostService;
use Core\Helpers;
use core\src\Http\Request;
use core\src\Http\Response;
use core\src\Http\Validator;

/**
 * Controller manage post actions from web routes
 */
class PostController
{

    /**
     * Get posts from model -> process result to view
     *
     * @return void
     */
    public function index(): void
    {
        $service = new PostService();

        $posts = $service->getAllPosts();

        Helpers\view('admin/index', compact('posts'));
    }

    /**
     * Process request to view with create form
     *
     * @return void
     */
    public function create(): void
    {
        Helpers\view('admin/create', compact('post'));
    }


    /**
     * Process request to view with edit form
     *
     * @param integer
     *
     * @return void
     */
    public function edit(int $id): void
    {
        $id = (int)$id;

        $service = new PostService();

        $post = $service->getPostById($id);

        Helpers\view('admin/edit', compact('post'));
    }


    /**
     * Process request add new post
     *
     * @param Request $request
     *
     * @return bool
     */
    public function append(Request $request): bool
    {
        $validator = new Validator($request);
        if (!$validator->check(['title', 'text'])) {
            return false;
        }

        $requestParams = $request->toObject();

        $service = new PostService();

        $status = $service->createPost($requestParams->title, $requestParams->text, $requestParams->author);

        if ($status) {
            $message = Response::message('Успешно добавлено', true);
        } else {
            $message = Response::message('Ошибка', false);
        }

        Helpers\view('admin/default', compact('status', 'message'));

        return $status;
    }

    /**
     * Process request update post
     *
     * @param Request $request
     *
     * @return bool
     */
    public function update(Request $request): bool
    {
        $validator = new Validator($request);
        if (!$validator->check(['title', 'text'])) {
            return false;
        }

        $requestParams = $request->toObject();

        $service = new PostService();

        $status = $service->updatePost($requestParams->title, $requestParams->text, $requestParams->author, $requestParams->id);

        if ($status) {
            $message = Response::message('Успешно обновлено', true);
        } else {
            $message = Response::message('Ошибка', false);
        }

        Helpers\view('admin/default', compact('status', 'message'));

        return $status;
    }


    /**
     * Process request delete post
     *
     * @param integer
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        $service = new PostService();

        $status = $service->deletePost($id);

        if ($status) {
            $message = Response::message('Успешно удалено', true);
        } else {
            $message = Response::message('Ошибка', false);
        }

        Helpers\view('admin/default', compact('status', 'message'));

        return $status;

    }
}