<?php

namespace App\Route;

use core\src\Http\Router;
use core\src\Http\Request;

use App\Controllers\Admin\PostController as AdminPostController;


/**
 * Route map for admin URI path of app
 *
 * @param Router $route
 * @param Request $request
 * @return void
 */
return function (Router $route, Request $request) {

    $route->route('/admin/posts/create', function ($matches) {
        $post = new AdminPostController();
        $post->create();
    });

    $route->route('/admin/posts/edit/:num', function ($matches) {
        $id = $matches[1][0];
        $post = new AdminPostController();
        $post->edit($id);
    });

    $route->route('/admin/posts/append', function () use ($request) {
        $post = new AdminPostController();
        $post->append($request);
    });

    $route->route('/admin/posts/update', function () use ($request) {
        $post = new AdminPostController();
        $post->update($request);
    });

    $route->route('/admin/posts/delete/:num', function ($matches) {
        $id = $matches[1][0];
        $post = new AdminPostController();
        $post->delete($id);
    });

    $route->route('/admin/posts', function ($matches) {
        $post = new AdminPostController();
        $post->index();

    });
};