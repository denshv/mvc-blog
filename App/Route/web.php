<?php

namespace App\Route;

use core\src\Http\Router;
use core\src\Http\Request;

use App\Controllers\PostController;

/**
 * Route map for web URI path of app
 *
 * @param Router $route
 * @return void
 */
return function (Router $route) {

    $route->route('/', function () {
        $post = new PostController;
        $post->index();
    });

    $route->route('/index', function () {

        $post = new PostController;
        $post->index();
    });

    $route->route('/posts', function () {
        $post = new PostController;
        $post->index();
    });

    $route->route('/posts/:num', function ($matches) {
        $id = $matches[1][0];
        $post = new PostController;
        $post->show($id);
    });
};

