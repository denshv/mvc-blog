<?php

namespace App\Models;

/**
 * Post Service mana
 */
class PostService
{

    private $post = null;

    public function __construct()
    {
        $this->post = new Post();
    }

    /**
     * Create Post
     *
     * @param string
     * @param string
     * @param string
     *
     * @return bool
     */
    public function createPost(string $title, string $text, string $author): bool
    {
        $this->post->build($title, $text, $author);

        return $this->post->insert();
    }

    /**
     * Update Post
     *
     * @param string
     * @param string
     * @param string
     * @param integer
     *
     * @return bool
     */
    public function updatePost(string $title, string $text, string $author, int $id): bool
    {
        $this->post->build($title, $text, $author, $id);

        return $this->post->update();
    }

    /**
     * Get all posts
     *
     * @return array
     */
    public function getAllPosts(): array
    {
        return $this->post->retrieve();
    }

    /**
     * Get post by id
     *
     * @param integer
     *
     * @return object|null
     */
    public function getPostById($id): object
    {
        $id = (int)$id;

        if (empty($id))
            return null;

        return $this->post->retrieveById($id);
    }

    /**
     * Delete post by id
     *
     * @param integer
     *
     * @return bool
     */
    public function deletePost($id): bool
    {
        $id = (int)$id;

        if (empty($id))
            return null;

        return $this->post->delete($id);
    }
}