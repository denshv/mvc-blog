<?php

namespace App\Models;

use core\Helpers;


/**
 * Post model
 */
class Post
{
    /**
     * Table name posts
     * @var string
     */
    private $table = 'posts';

    /**
     * Post id
     * @var integer
     */
    public $id;

    /**
     * Post title
     * @var string
     */
    public $title;

    /**
     * Post text
     * @var string
     */
    public $text;

    /**
     * Post author
     * @var string
     */
    public $author;

    /**
     * PDO instance
     * @var \PDO
     */
    private $db = null;

    /**
     * Required fields for insert|update
     * @var \PDO
     */
    protected $required = ['title', 'text', 'author'];

    public function __construct()
    {
        $this->db = Helpers\getDB();
    }

    /**
     * Set up params for Post
     * @param string
     * @param string
     * @param string
     * @param integer
     *
     * @return  Post
     */
    public function build(string $title, string $text, string $author, int $id = 0): Post
    {
        $this->title = $title;
        $this->text = $text;
        $this->author = $author;
        $this->id = $id;

        return $this;
    }

    /**
     * Get posts from database
     *
     * @return  array
     */
    public function retrieve(): array
    {

        return $this->db->query("SELECT * FROM {$this->table}")->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Get posts from database
     *
     * @param int
     * @return  object
     */
    public function retrieveById($id): object
    {

        $stmt = $this->db->prepare("SELECT * FROM {$this->table} WHERE id = :id");
        $stmt->execute(array('id' => $id));

        foreach ($stmt as $row) {
            return (object)$row;
        }

        return null;
    }

    /**
     * Update post
     *
     * @return  bool
     */
    public function update(): bool
    {

        $query = [];
        foreach ($this->required as $param) {
            $query[] = " `{$param}` = :{$param} ";
        }

        $queryStr = implode(',', $query);

        $stmt = $this->db->prepare("UPDATE {$this->table} SET {$queryStr} WHERE id = :id");

        $stmt->execute(['id' => $this->id, 'title' => $this->title, 'text' => $this->text, 'author' => $this->author]);

        if ($stmt->rowCount())
            return true;
        else
            return false;
    }

    /**
     * Insert post
     *
     * @return  bool
     */
    public function insert(): bool
    {

        $query = [];

        foreach ($this->required as $param) {
            $query[] = " `{$param}` = :{$param} ";
        }

        $queryStr = implode(',', $query);

        $stmt = $this->db->prepare("INSERT INTO {$this->table} SET {$queryStr}");

        $r = $stmt->execute(['title' => $this->title, 'text' => $this->text, 'author' => $this->author]);

        return $r;
    }

    /**
     * Delete post
     *
     * @param integer
     * @return  bool
     */
    public function delete($id): bool
    {

        $stmt = $this->db->prepare("DELETE FROM {$this->table} WHERE id = :id");
        $stmt->execute(['id' => $id]);

        if ($stmt->rowCount())
            return true;
        else
            return false;
    }
}