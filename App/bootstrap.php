<?php

namespace App;

require_once BASE_PATH . '/core/autoload.php';
require_once BASE_PATH . '/core/helpers.php';

use core\src\Http\Router;
use core\src\Http\Request;
use core\Helpers;

/**
 * Function init app.
 *
 * Init database.
 * Process Request
 * Process Request to Router
 * Set corrent route map
 *
 * @return void
 */

function run()
{

    Helpers\initDB(DB_USER, DB_PASS, DB_HOST, DB_NAME);

    $request = new Request();
    $route = new Router($request);

    $routePath =
        [
            'admin' => __DIR__ . '/Route/admin.php',
            'web' => __DIR__ . '/Route/web.php'
        ];

    if ($route->isAdmin()) {
        $route->processMap($routePath['admin']);
    } else {
        $route->processMap($routePath['web']);
    }
}