<?php

require_once __DIR__ . '/config.php';
require_once BASE_PATH . '/core/autoload.php';
require_once BASE_PATH . '/core/helpers.php';

use core\Helpers;

$createSql = file_get_contents(BASE_PATH . '/App/DataBase/Migrations/init.sql');
$insertSql = file_get_contents(BASE_PATH . '/App/DataBase/Seeds/insert.sql');

$db = Helpers\initDB(DB_USER, DB_PASS, DB_HOST, DB_NAME);

$db->query($createSql);
$db->query($insertSql);

die('app database init - ok.'.PHP_EOL);